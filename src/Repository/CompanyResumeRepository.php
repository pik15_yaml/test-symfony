<?php

namespace App\Repository;

use App\Entity\CompanyResume;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method CompanyResume|null find($id, $lockMode = null, $lockVersion = null)
 * @method CompanyResume|null findOneBy(array $criteria, array $orderBy = null)
 * @method CompanyResume[]    findAll()
 * @method CompanyResume[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CompanyResumeRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, CompanyResume::class);
    }

    public function getReactions($id, $type)
    {
        return $this->createQueryBuilder('cr')
            ->select('COUNT(cr.reaction)')
            ->where('cr.resumeId = :id')
            ->andWhere('cr.reaction = :reaction')
            ->setParameter('id', $id)
            ->setParameter('reaction', $type)
            ->getQuery()
            ->getSingleScalarResult()
            ;
    }

    // /**
    //  * @return CompanyResume[] Returns an array of CompanyResume objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('c.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?CompanyResume
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
