<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20191120232628 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE IF EXISTS `user`;
                            CREATE TABLE `user` (
                              `id` int(11) NOT NULL AUTO_INCREMENT,
                              `username` varchar(180) COLLATE utf8mb4_unicode_ci NOT NULL,
                              `roles` json NOT NULL,
                              `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
                              PRIMARY KEY (`id`),
                              UNIQUE KEY `UNIQ_8D93D649F85E0677` (`username`)
                            ) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;');


        $this->addSql('DROP TABLE IF EXISTS `company`;
                            CREATE TABLE `company` (
                              `id` int(11) NOT NULL AUTO_INCREMENT,
                              `user_id` int(11) NOT NULL,
                              `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
                              `site` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
                              `address` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
                              `phone` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
                              PRIMARY KEY (`id`),
                              KEY `fk_user` (`user_id`),
                              CONSTRAINT `fk_user` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
                            ) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;');


        $this->addSql('DROP TABLE IF EXISTS `resume`;
                            CREATE TABLE `resume` (
                              `id` int(11) NOT NULL AUTO_INCREMENT,
                              `user_id` int(11) NOT NULL,
                              `position` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
                              `text` varchar(5000) COLLATE utf8mb4_unicode_ci NOT NULL,
                              `created_at` datetime NOT NULL,
                              `updated_at` datetime NOT NULL,
                              PRIMARY KEY (`id`),
                              KEY `resume_fk_user` (`user_id`),
                              CONSTRAINT `resume_fk_user` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
                            ) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;');

        $this->addSql('DROP TABLE IF EXISTS `company_resume`;
                            CREATE TABLE `company_resume` (
                              `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
                              `company_id` int(11) NOT NULL,
                              `resume_id` int(11) NOT NULL,
                              `sent_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
                              `reaction` int(11) DEFAULT NULL,
                              PRIMARY KEY (`id`),
                              KEY `fk_company` (`company_id`),
                              KEY `fk_resume` (`resume_id`),
                              CONSTRAINT `fk_company` FOREIGN KEY (`company_id`) REFERENCES `company` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
                              CONSTRAINT `fk_resume` FOREIGN KEY (`resume_id`) REFERENCES `resume` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
                            ) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE resume');
        $this->addSql('DROP TABLE user');
        $this->addSql('DROP TABLE company');
        $this->addSql('DROP TABLE company_resume');
    }
}
