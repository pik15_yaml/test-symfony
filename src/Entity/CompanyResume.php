<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\CompanyResumeRepository")
 */
class CompanyResume
{
    const REACTION_POSITIVE = 2;
    const REACTION_NEGATIVE = 1;
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="integer")
     */
    private $companyId;

    /**
     * @ORM\Column(type="integer")
     */
    private $resumeId;

    /**
     * @ORM\Column(type="datetime")
     */
    private $sent_at;

    /**
     * @ORM\Column(type="integer")
     */
    private $reaction;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getSentAt(): ?\DateTimeInterface
    {
        return $this->sent_at;
    }

    public function getsent_at(): ?\DateTimeInterface
    {
        return $this->sent_at;
    }

    public function setSentAt(\DateTimeInterface $sent_at): self
    {
        $this->sent_at = $sent_at;

        return $this;
    }

    public function getReaction(): ?string
    {
        $reaction = '';
        if ($this->reaction == self::REACTION_POSITIVE) {
            $reaction = 'POSITIVE';
        } elseif ($this->reaction == self::REACTION_NEGATIVE) {
            $reaction = 'NEGATIVE';
        }
        return $reaction;
    }

    public function setReaction(int $reaction): self
    {
        $this->reaction = $reaction;

        return $this;
    }

    public function getResumeId(): ?int
    {
        return $this->resumeId;
    }

    public function setResumeId(int $resumeId): self
    {
        $this->resumeId = $resumeId;

        return $this;
    }

    public function getCompanyId(): ?int
    {
        return $this->companyId;
    }

    public function setCompanyId(int $companyId): self
    {
        $this->companyId = $companyId;

        return $this;
    }
}
