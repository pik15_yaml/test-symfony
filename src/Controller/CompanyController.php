<?php

namespace App\Controller;

use App\Entity\Company;
use App\Entity\CompanyResume;
use App\Entity\Resume;
use App\Form\CompanyType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

class CompanyController extends AbstractController
{
    public function index()
    {
        return $this->render('employer/index.html.twig', [
            'controller_name' => 'EmployerController',
        ]);
    }

    public function employerCompanies()
    {
        $em = $this->getDoctrine()->getManager();
        $user = $this->get('security.token_storage')->getToken()->getUser();


        $companies = $em->getRepository(Company::class)->findBy(['user_id' => $user->getId()]);

        return $this->render('employer/employer-companies.html.twig', [
            'controller_name' => 'EmployerController',
            'companies' => $companies,
        ]);
    }

    public function createCompany(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $user = $this->get('security.token_storage')->getToken()->getUser();

        $company = new Company();
        $form = $this->createForm(CompanyType::class, $company);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $company->setUserId($user->getId());

            $em->persist($company);
            $em->flush();

            return $this->redirectToRoute('employer_companies');
        }

        return $this->render('employer/create-company.html.twig', [
            'controller_name' => 'EmployerController',
            'form' => $form->createView(),
        ]);
    }

    public function deleteCompany(Company $company)
    {
        $em = $this->getDoctrine()->getManager();
        $em->remove($company);
        $em->flush();

        return $this->redirectToRoute('employer_companies');
    }

    public function updateCompany(Company $company, Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $form = $this->createForm(CompanyType::class, $company);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em->persist($company);
            $em->flush();
            return $this->redirectToRoute('employer_companies');
        }

        return $this->render('employer/create-company.html.twig', [
            'controller_name' => 'EmployerController',
            'form' => $form->createView(),
        ]);
    }

    public function viewCompany(Company $company)
    {
        return $this->render('employer/view-company.html.twig', [
            'controller_name' => 'EmployerController',
            'company' => $company,
        ]);
    }

    public function bids()
    {
        $em = $this->getDoctrine()->getManager();
        $user = $this->get('security.token_storage')->getToken()->getUser();
        $companies = $em->getRepository(Company::class)->findBy(['user_id' => $user->getId()]);
        $history = $em->getRepository(CompanyResume::class)->findBy(['companyId' => $companies]);

        $filteredHistory = array();
        foreach ($history as $item) {
            $filteredHistory[$item->getCompanyId()][$item->getResumeId()] = $item;
        }

        return $this->render('employer/bids.html.twig', [
            'controller_name' => 'EmployerController',
            'companies' => $companies,
            'history' => $filteredHistory
        ]);
    }

    public function ajaxSelectReaction(Request $request)
    {
        if ($request->isXMLHttpRequest()) {
            $em = $this->getDoctrine()->getManager();
            $id = $request->get('id');
            $reaction = $request->get('reaction');
            $model = $em->getRepository(CompanyResume::class)->findOneById($id);
            $model->setReaction(intval($reaction));
            $em->persist($model);
            $em->flush();

            return new JsonResponse(array('result' => true));
        }
    }
}
