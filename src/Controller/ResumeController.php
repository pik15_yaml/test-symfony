<?php

namespace App\Controller;

use App\Entity\Company;
use App\Entity\CompanyResume;
use App\Entity\Resume;
use App\Form\ResumeType;
use App\Repository\ResumeRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;


class ResumeController extends AbstractController
{
    public function index(ResumeRepository $resumeRepository): Response
    {
        $user = $this->get('security.token_storage')->getToken()->getUser();
        return $this->render('resume/index.html.twig', [
            'resumes' => $resumeRepository->findBy(['user_id' => $user->getId()]),
        ]);
    }

    public function new(Request $request): Response
    {
        $resume = new Resume();
        $form = $this->createForm(ResumeType::class, $resume);
        $form->handleRequest($request);
        $user = $this->get('security.token_storage')->getToken()->getUser();

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $resume->setUserId($user->getId());
            $date = new \Datetime();
            $resume->setCreatedAt($date);
            $resume->setUpdatedAt($date);
            $entityManager->persist($resume);
            $entityManager->flush();

            return $this->redirectToRoute('resume_index');
        }

        return $this->render('resume/new.html.twig', [
            'resume' => $resume,
            'form' => $form->createView(),
        ]);
    }

    public function show(Resume $resume): Response
    {
        return $this->render('resume/show.html.twig', [
            'resume' => $resume,
        ]);
    }

    public function edit(Request $request, Resume $resume): Response
    {
        $form = $this->createForm(ResumeType::class, $resume);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $resume->setUpdatedAt(new \DateTime());
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('resume_index');
        }

        return $this->render('resume/edit.html.twig', [
            'resume' => $resume,
            'form' => $form->createView(),
        ]);
    }

    public function delete(Resume $resume)
    {
        $em = $this->getDoctrine()->getManager();
        $em->remove($resume);
        $em->flush();

        return $this->redirectToRoute('resume_index');
    }

    public function sendResume(Request $request, Company $company)
    {
        $em = $this->getDoctrine()->getManager();
        $user = $this->get('security.token_storage')->getToken()->getUser();

        $sendResumes = $company->getResumes();
        $resume = $em->getRepository(Resume::class)->findBy(['user_id' => $user->getId()]);
        $notSendResume = [];
        foreach ($resume as $item) {
            if (!in_array($item, $sendResumes->getValues())) {
                $notSendResume[] = $item;
            }
        }

        if ($request->getMethod() == Request::METHOD_POST) {
            $values = $request->request->get('resume');
            $resumeSend = $em->getRepository(Resume::class)->findById($values);

            foreach ($resumeSend as $resume) {
                $company->addResume($resume);
                $this->getDoctrine()->getManager()->flush();
            }
            return $this->redirectToRoute('list_companies');
        }

        return $this->render('resume/send-resume.html.twig', [
            'resumes' => $notSendResume,
            'company' => $company
        ]);
    }

    public function reviewHistory()
    {
        $em = $this->getDoctrine()->getManager();
        $user = $this->get('security.token_storage')->getToken()->getUser();
        $resumes = $em->getRepository(Resume::class)->findBy(['user_id' => $user->getId()]);
        $history = $em->getRepository(CompanyResume::class)->findBy(['resumeId' => $resumes]);

        $filteredHistory = array();
        foreach ($history as $item) {
            $filteredHistory[$item->getResumeId()][$item->getCompanyId()] = $item;
        }

        return $this->render('resume/review-history.html.twig', [
            'resumes' => $resumes,
            'history' => $filteredHistory
        ]);

    }
}
