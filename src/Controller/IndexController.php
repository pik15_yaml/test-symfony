<?php

namespace App\Controller;

use App\Entity\Company;
use App\Entity\CompanyResume;
use App\Entity\Resume;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class IndexController extends AbstractController
{
    public function index()
    {
        return $this->render('index/index.html.twig', [
            'controller_name' => 'IndexController',
        ]);
    }

    public function listCompanies()
    {
        $em = $this->getDoctrine()->getManager();

        $companies = $em->getRepository(Company::class)->findAll();

        return $this->render('index/list-companies.html.twig', [
            'controller_name' => 'IndexController',
            'companies' => $companies,
        ]);
    }

    public function listResume()
    {
        $em = $this->getDoctrine()->getManager();

        $resume = $em->getRepository(Resume::class)->findAll();

        return $this->render('index/list-resume.html.twig', [
            'controller_name' => 'IndexController',
            'resume' => $resume,
        ]);
    }

    public function topResumes()
    {
        $em = $this->getDoctrine()->getManager();

        $resumes = $em->getRepository(Resume::class)->findAll();
        $rating = array();
        $reactions = array();
        foreach ($resumes as $key => $resume) {
            $positives = $em->getRepository(CompanyResume::class)->getReactions($resume->getId(), CompanyResume::REACTION_POSITIVE);
            $negatives = $em->getRepository(CompanyResume::class)->getReactions($resume->getId(), CompanyResume::REACTION_NEGATIVE);
            $reactions[$resume->getId()]['positive'] = $positives;
            $reactions[$resume->getId()]['negative'] = $negatives;
            $rating[$key] = $positives - $negatives;
        }
        arsort($rating);
        $keys = array_keys($rating);
        $descResumes = array();
        for ($i = 0; $i < count($resumes); $i++) {
            $descResumes[] = $resumes[array_shift($keys)];
        }

        return $this->render('index/top-resumes.html.twig', [
            'controller_name' => 'IndexController',
            'resumes' => $descResumes,
            'reactions' => $reactions
        ]);
    }
}
