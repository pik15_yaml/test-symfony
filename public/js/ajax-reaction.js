$( document ).ready(function() {

    const POSITIVE = '2';
    const NEGATIVE = '1';
    var id;
    $( ".btn-modal" ).click(function() {
        id = $(this).val();
        $(this).addClass('last');
    });

    jQuery("#reaction-form").on('submit', function (event) {
        event.preventDefault();
        event.stopImmediatePropagation();
        var data = [];
        var reaction = $("select#reaction").val();
        $.ajax({
            type: 'POST',
            url: '/ajax-select-reaction',
            data: {'reaction' : reaction, 'id' : id},
            encode: true,
            success: function () {
                $(".close").click();
                var text = (reaction === POSITIVE) ? 'POSITIVE' : 'NEGATIVE';
                $('.last').text(text);
                $('.last').removeClass('last');
            },
        }).fail(function () {
            $('#result').html('<div class="alert alert-danger">Fail</div>');
        });

        return false;
    });
});

