<?php

use App\Controller\CompanyController;
use App\Controller\ResumeController;
use App\Controller\SecurityController;
use Symfony\Component\Routing\RouteCollection;
use Symfony\Component\Routing\Route;
use App\Controller\IndexController;

$routes = new RouteCollection();
$routes->add('index', new Route('/', [
    '_controller' => [IndexController::class, 'index']
]));
$routes->add('user_registration', new Route('/register', [
    '_controller' => [SecurityController::class, 'register']
]));
$routes->add('user_login', new Route('/login', [
    '_controller' => [SecurityController::class, 'login']
]));
$routes->add('logout', new Route('/logout', [
    '_controller' => [SecurityController::class, 'logout']
]));
$routes->add('employer_companies', new Route('/employer-companies', [
    '_controller' => [CompanyController::class, 'employerCompanies']
]));
$routes->add('create_company', new Route('/create-company', [
    '_controller' => [CompanyController::class, 'createCompany']
]));
$routes->add('delete_company', new Route('/delete-company/{company}', [
    '_controller' => [CompanyController::class, 'deleteCompany']
]));
$routes->add('view_company', new Route('/view-company/{company}', [
    '_controller' => [CompanyController::class, 'viewCompany']
]));
$routes->add('update_company', new Route('/update-company/{company}', [
    '_controller' => [CompanyController::class, 'updateCompany']
]));
$routes->add('list_companies', new Route('/list-companies', [
    '_controller' => [IndexController::class, 'listCompanies']
]));
$routes->add('resume_index', new Route('/worker-resume', [
    '_controller' => [ResumeController::class, 'index']
]));
$routes->add('create_resume', new Route('/create-resume', [
    '_controller' => [ResumeController::class, 'new']
]));
$routes->add('delete_resume', new Route('/delete-resume/{resume}', [
    '_controller' => [ResumeController::class, 'delete']
]));
$routes->add('view_resume', new Route('/view-resume/{resume}', [
    '_controller' => [ResumeController::class, 'show']
]));
$routes->add('update_resume', new Route('/update-resume/{resume}', [
    '_controller' => [ResumeController::class, 'edit']
]));
$routes->add('list_resume', new Route('/list-resume', [
    '_controller' => [IndexController::class, 'listResume']
]));
$routes->add('send_resume', new Route('/send-resume/{company}', [
    '_controller' => [ResumeController::class, 'sendResume']
]));
$routes->add('review_history', new Route('/review-history', [
    '_controller' => [ResumeController::class, 'reviewHistory']
]));
$routes->add('bids', new Route('/bids', [
    '_controller' => [CompanyController::class, 'bids']
]));
$routes->add('ajax_select_reaction', new Route('/ajax-select-reaction', [
    '_controller' => [CompanyController::class, 'ajaxSelectReaction']
]));
$routes->add('top_resume', new Route('/top-resumes', [
    '_controller' => [IndexController::class, 'topResumes']
]));
return $routes;